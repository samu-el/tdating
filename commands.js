const Markup = require('telegraf/markup');

settings = ({ reply }) => {
    return reply('Choose A Setting Below', Markup
      .keyboard([
        ['🧑🏽 Profile', '🇺🇸 🇪🇹 Language'],
        ['💓 Matches', '📞 Feedback'], 
      ])
      .oneTime()
      .resize()
      .extra());
}

module.exports = {
    settings,
}