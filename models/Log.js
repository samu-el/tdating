const mongoose = require('mongoose');

const Log = mongoose.Schema({
    update: String,
    type: String,
    created_at: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Log', Log);