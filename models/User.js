const mongoose = require('mongoose');

const User = mongoose.Schema({
    id: Number,
    //Default Telegram Properties
    telegram_id: Number,
    username: String,
    first_name: String,
    last_name: String,
    is_bot: Boolean,
    language_code: String,
    //Additional Needed Properties
    age: Number,
    sex: String,
    location: String,
    //Additional Logging info
    created_at: { type: Date, Default: Date.now },
    updated_at: Date,
});

User.pre('save', function(next) {
    this.updated_at = new Date();
    
    if (!this.created_at)
      this.created_at = this.updated_at;

    next();
});

module.exports = mongoose.model('User', User);