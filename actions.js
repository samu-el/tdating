const Markup = require('telegraf/markup');
const Log = require('./models/Log');
const User = require('./models/User');


upload = (ctx, next) => {
    return ctx.editMessageText('Please Upload your photo');
    //TODO: Receive uploaded photo
}

sex = (ctx, next) => {
    return ctx.editMessageText('Please Choose your sex',
        Markup.inlineKeyboard([
        Markup.callbackButton('Male', 'Male'),
        Markup.callbackButton('Female', 'Female'),
        ]).extra());
}

male =  (ctx, next) => {
    //TODO:
    User.findOneAndUpdate({ telegram_id: ctx.update.callback_query.from.id }, { sex: 'male' })
            .then()
            .catch()
    return ctx.editMessageText('You chose 👨', Markup.inlineKeyboard([
        Markup.callbackButton('Go Next Setting', 'Next')
        ]).extra()).then(() => next());
}

female =(ctx, next) => {
    //TODO:
    User.findOneAndUpdate({ telegram_id: ctx.update.callback_query.from.id }, { sex: 'female' })
            .then()
            .catch()
    return ctx.editMessageText('You chose 👩', Markup.inlineKeyboard([
        Markup.callbackButton('Go Next Setting', 'Next')
        ]).extra()).then(() => next());
}

age = (ctx, next) => {
    return ctx.editMessageText('Please Enter your age');
}

location = (ctx, next) => {
    return ctx.editMessageText('Please Enter Your location');
}

nextSetting = (ctx, next) => {
    ctx.reply('//TODO: return an appropriate setting to fill');
}

module.exports  = {
    upload,
    sex,
    male,
    female,
    age,
    location,
    nextSetting
};