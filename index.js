require('dotenv').config();

const Telegraf = require('telegraf')
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');

const imgur = require('imgur');
const mongoose = require('mongoose');

const Log = require('./models/Log');
const User = require('./models/User');

const commands = require('./commands');
const actions = require('./actions');
const listeners = require('./listeners');

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true})

const bot = new Telegraf(process.env.BOT_TOKEN)

bot.use(Telegraf.log());
bot.use((ctx, next) => {
    let newLog = new Log({type:"middleware", update:JSON.stringify(ctx.update)});
    newLog.save()
        .then(() => {
            next();
        })
        .catch((err) => {
            console.log("middleware Error SAVE ERROR:", err);
            next();
        });
})

bot.telegram.getMe().then((bot_informations) => {
    bot.options.username = bot_informations.username;
    console.log("Server has initialized. Bot:", bot_informations.username);
}).catch( error => {
    console.log("Start Error:", error);
    let newLog = new Log({type:"getMeError", update:JSON.stringify(error)});
    newLog.save()
        .catch((err) => {
            console.log("getMeError SAVE ERROR:", err);
        });
});

//On Start Command -- Should the user add a bio, nickname, and age?
bot.start((ctx) => {
    //TODO: Chek if the users is a new one or an existing one    id: Number,
    User.findOne({telegram_id: ctx.message.from.id}).exec()
            .then( user => {
                console.log("START", user);
                if(user){ //User Exists
                    ctx.reply(`Welcome Back ${user.first_name}!`, 
                        Markup.inlineKeyboard([[
                            Markup.callbackButton('My Matches', 'Matches'),
                            Markup.callbackButton('Browse', 'Browse'),
                            Markup.callbackButton('Settings', 'Settings'),                    
                        ]]).extra())
                }
                else {
                    let newUser = new User({
                        telegram_id: ctx.message.from.id,
                        username: ctx.message.from.username,
                        first_name: ctx.message.from.first_name,
                        last_name: ctx.message.from.last_name,
                        is_bot: ctx.message.from.is_bot,
                        language_code: ctx.message.from.language_code,
                    })

                    newUser.save()
                                .then(() => {
                                    ctx.reply('Welcome! What do you want to do first? You need to fill these before we begin.', 
                                        Markup.inlineKeyboard([[
                                            Markup.callbackButton('Choose Sex', 'Sex'),
                                            Markup.callbackButton('Upload Photo', 'Upload'),
                                        ],[
                
                                            Markup.callbackButton('Set Age', 'Age'),
                                            Markup.callbackButton('Select Location', 'Location'),
                                        ]]).extra())
                                })
                                .catch( (err) => {
                                    console.log("Start Error:", err);
                                    ctx.reply('Something Went Wrong! Please notify the bot admins')
                                    let newLog = new Log({type:"AddUser", update:JSON.stringify(err)});
                                    newLog.save()
                                        .catch((err) => {
                                            console.log("AddUser SAVE ERROR:", err);
                                        });
                                })
                }
            })
            .catch( (err) => {
                console.log("Start Error:", err);
                let newLog = new Log({type:"/StartError",update:JSON.stringify(err)});
                newLog.save()
                    .catch((err) => {
                        console.log("SAVE ERROR:", err);
                    });
            })
});

//On Settings command
bot.command('settings', commands.settings);

//ACTIONS
bot.action('Sex', actions.sex);
bot.action('Upload', actions.upload);
bot.action('Age', actions.age);
bot.action('Location', actions.location);
bot.action('Next', actions.nextSetting)

//SUBACTIONS
bot.action('Male', actions.male)
bot.action('Female', actions.female)

//LISTENERS
bot.hears('🧑🏽 Profile', listeners.profile);
bot.hears('💓 Matches', listeners.matches);
bot.hears('🇺🇸 🇪🇹 Language', listeners.language);
bot.hears('📞 Feedback', listeners.feedback);


//TESTING IMGUR UPLOAD
bot.on('photo', ctx => {
    const photo = ctx.message.photo[2] || ctx.message.photo[1]
    ctx.telegram.getFileLink(photo.file_id)
        .then((url) =>{
            console.log("URL:", url)
            imgur.uploadUrl(url)
                    .then( response => {
                        console.log("IMGUR:", response)
                        ctx.reply(response.data.link, Markup.inlineKeyboard([
                            Markup.callbackButton('❤️', 'Like'),
                            Markup.callbackButton('❌', 'Dislike')
                            ]).extra())
                    })
        })
})

bot.startPolling();
